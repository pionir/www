---
title: "Шта је пионирима доступно од опреме?"
date: "2020-02-16"
description: ""
---
Велики број пионира није имало идеалне услове за рад када су почели са учењем. Стари рачунари, телефони, недостатак друге опреме и непостојећа материјална средства нису изговор да се одустане од учења. У Школи зато сви улажемо у заједничке ресурсе који су доступни за коришћење свима, како би се пребродио први период њиховог учења. Такође, како стичеш знање, можеш да очекујеш да ћеш моћи да зарадиш за сву опрему која ти је потребна.

Школа поседује и заједничке ресурсе коју можеш видети на страни Чиме располажемо.