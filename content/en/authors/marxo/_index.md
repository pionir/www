---
title: 'Marko Kažić'
date: 2018-11-19T10:47:58+10:00
---

Marko Kažić is the founder of [Zamphyr](https://zamphyr.com), a free school of technology. Pioneer of Free Education and the Education Movement 2.0. He works on exponential radical ideas and invests in extraordinary people. As an activist for free culture and free education, he advocates a fundamental paradigm shift in education and the development of a non-sparce society.
