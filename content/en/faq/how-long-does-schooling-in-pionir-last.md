---
title: "How long does schooling in Pionir last?"
date: "2020-02-16"
description: ""
---
Education in Pionir is not limited. In line with the philosophy of free education, we believe that lifelong learning is the only right approach to education. As pionirs grow and progress in life, their approach, activities and engagement in Pionir change, but remain a significant part of the community, passing on their knowledge to younger generations as they acquire new, more advanced knowledge for themselves.
