---
title: "How is Pionir free?"
date: "2020-02-16"
description: ""
---
Pionir is free because we build, create and develop the value and resources of the school together. This means that the value we draw from school must be created together. To have the equipment to work, we rely on fundraising and donations, to pay bills and running costs, we rely on self-contribution.

Every pionir participates in common obligations that enable us to build, maintain and improve our school, because in order to have a school from which we draw, we must also participate in creating its value.