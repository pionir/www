---
title: "How much time should I set aside for Pionir?"
date: "2020-02-16"
description: ""
---

An average of 10 hours per week. Keep in mind that the more time you invest and the more regularly and consistently you participate in activities and learn, the greater your outcomes and successes will be. It is important to know that without 10 hours a week dedicated to work and study, it does not mean that you should give up other extracurricular activities, all pionirs before you who have succeeded are proof that time can be planned so that other activities do not hinder your progress in Pionir .
