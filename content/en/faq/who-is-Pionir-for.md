---
title: "Who is Pionir for? Do I need any prior knowledge?"
date: "2020-02-16"
description: ""
---

Pionir Free School is open to students aged 13 to 17, primarily to students in grades VII and VIII of primary schools, and students in grades I, II and III of secondary schools. Most importantly, Pionir is for those who want to learn and work more, for the curious, for the creative or for those who believe that their work and knowledge can change the world, regardless of success and grades in school. Pre-knowledge is not necessary, because in Pionir we always start from the basics and we do not assume that you know something in advance. 