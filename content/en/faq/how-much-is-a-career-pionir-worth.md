---
title: "How much is a career pionir worth?"
date: "2020-02-16"
description: ""
---

Since all work at Pionir focuses on practice, state-of-the-art technologies and projects, pionirs have a unique advantage in further education and career. A pionir is an opportunity to acquire practical knowledge at a very young age, build a portfolio of projects, be a part of a noticeable social change and thus put yourself ahead of others for years.
