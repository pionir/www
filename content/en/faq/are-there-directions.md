---
title: "Are there directions?"
date: "2020-02-16"
description: ""
---
The pionir, like any free school, is individualized. This means that there is no direction in which you enroll, but the direction and structure is created while you study, just for you. This ensures that you will learn in the best possible way, in accordance with your affinities and abilities.
