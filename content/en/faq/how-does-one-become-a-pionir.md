---
title: "How does one become a pionir?"
date: "2020-02-16"
description: ""
---
Pionir Free School accepts new candidates every spring and fall. You can become a pionir if the call is open by logging in to the link. Read more on the Become a Pionir page. By applying, you become a candidate for pionir, who in the next at least four months will learn the basics of technology, society, ethics and related topics, so that we can equip you for further progress in the digital society and lay the foundations for your further work. When you successfully fulfill all the tasks we put before the candidates, which are mainly understanding the basic concepts, understanding the culture of the school and the way of working, you become a pionir.
