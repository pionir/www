---
title: "Can I switch to another topic if I don't like the topic I initially chose?"
date: "2020-02-16"
description: ""
---
Of course, that is the beauty of freedom! If you decide to have a topic, e.g. programming is not for you, and if you are interested in design, it is completely understandable that you start learning and doing design, and mentors, other pionirs and the School will help you with that. 
