---
title: "Do we buy textbooks and how much do they cost?"
date: "2020-02-16"
description: ""
---
Not. As part of our philosophy of free education, we believe that textbooks as materials for acquiring knowledge should be free and accessible to all. We treat knowledge as a legacy and are happy to share it with everyone. That is why we strive to learn from all publicly available resources, whether digital or printed, and we strive to develop free and open textbooks for the benefit of all mankind.
