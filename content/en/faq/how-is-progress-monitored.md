---
title: "How is progress monitored?"
date: "2020-02-16"
description: ""
---
Your progress is monitored in several dimensions. First, we monitor whether your abilities are growing and to what extent you can turn what you are learning into something useful in society. Secondly, there are other values ​​that are important for progress, from how you cooperate with your friends, to participating in our common obligations at school. In fact, progress for us is when we see that you are a healthier, more solidary, more involved and more capable member of society - and that you use your abilities for the benefit of all.
