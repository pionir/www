---
title: "What do I need from learning and working equipment?"
date: "2020-02-16"
description: ""
---
Ideally, you have your own laptop. If you don't have one, you definitely need a computer that is relatively new and capable of performing the tasks you use it for. Of course, we understand that not everyone is able to afford a laptop or computer. The school has a limited number of computers available to students for work, in case they do not have their own computers.

In addition to the equipment itself, it is extremely important to have a corner in your house, where you can work in peace without interruption, 1-2 hours a day.
