---
title: "What is the difference between a Pionir and an ordinary school?"
date: "2020-02-16"
description: ""
---
The difference between Pionir and your classical school is great. There are no grades, exams, tests, transcripts in Pionir. Most activities are subordinated to projects, work and creation, above all the practical application of everything you learn. There are no classes, so there are no absences, but a good part of success is in regular learning and attendance. In Pionir you get the freedom and responsibility that comes with it.

The pionir is special. In Pionir, in addition to learning, we work and create together, from projects to the school itself, we organize work actions and take care of our environment and community. 