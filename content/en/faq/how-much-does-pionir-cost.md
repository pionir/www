---
title: "How much does Pionir cost?"
date: "2020-02-16"
description: ""
---
One of our basic beliefs is that money must not be an obstacle to every human being for quality education. That is why attending Pionir is free, there is no membership fee and no attendance fee.
