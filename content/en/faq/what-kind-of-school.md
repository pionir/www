---
title: "What kind of school is Pionir?"
date: "2020-02-16"
description: ""
---

Pionir is a free school, which aims to teach you about technology, design, communication, ethics and society, through work aimed at turning knowledge of new technologies into innovation, and exciting and interesting projects that will improve the world around us.

