---
title: "What equipment is available to pionirs?"
date: "2020-02-16"
description: ""
---
Many pionirs did not have ideal working conditions when they started learning. Old computers, telephones, lack of other equipment and non-existent material resources are not an excuse to give up learning. At the School, therefore, we all invest in shared resources that are available for use by all, in order to get through the first period of their learning. Also, as you gain knowledge, you can expect to be able to earn money for all the equipment you need.

The school also has common resources, which you can see on the What we have page.