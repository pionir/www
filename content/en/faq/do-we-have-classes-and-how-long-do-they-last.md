---
title: "Do we have classes and how long do they last?"
date: "2020-02-16"
description: ""
---
We don't have classes. The school is open every day of the week, and students use it for equipment or as a space for collaboration and exchange of ideas. Depending on the activities, it can happen that the actions are outside the school premises, or that the lectures, when experts from a topic come to us, are limited in time. However, you will rarely see something like a school lesson, where you sit for 45 minutes and listen and record.
