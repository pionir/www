---
title: "What is the space we work in?"
date: "2020-02-16"
description: ""
---

The school is located in the building of a traditional Banat family house more than 200 years old (from 1814), which we converted into a school, built classrooms, laboratories, a warehouse, a multimedia room and a library. The pionirs worked tirelessly for more than 4 months in the restoration and remodeling of the space. We are extremely proud to have built our own school and hope to expand our capacity in the future and increase the space for even more pionirs. 

The school also has common resources, which you can see on the What we have page.