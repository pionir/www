---
title: "What does learning in Pionir look like?"
date: "2020-02-16"
description: ""
---
At Pionir, we believe in self-education, which means that we do not impose a methodology or a way of learning, and you choose the topics and direction of your learning. There is no desirable or undesirable knowledge and we do not decide whether some knowledge is valuable or not, our goal is to direct all acquired knowledge towards building yourself as a person, a free and educated person and to apply that knowledge for the benefit of society.

However, we believe in the key value of teachers and mentors, and their knowledge, and they are there to be co-creators of your education, to provide you with guidance, support and to design didactic tools and activities that will guide you on your path to enlightenment.

In the beginning, when you learn more about basic concepts and practices, you are expected to read, watch and listen to materials at home that will give you knowledge of these basics so that you can apply them as soon as possible. As time passes, more and more practice comes to the fore and you gain new knowledge through projects. Most of the time in Pionir is the time you will spend doing something new, working on a project, or on an action.