---
title: "Aaron Swartz"
description: "Aaron Hillel Swartz was an American computer programmer, entrepreneur, writer, political organizer, and Internet hacktivist."
---